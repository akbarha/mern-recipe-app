import express from 'express'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import UserModel from '../models/user.js'

const router = express.Router();

router.post('/register', async(req,res) => {
    const {username, password} = req.body;
    const user = await UserModel.findOne({username:username});

    // kalau ada user gak bisa lah, udah ada soalnya. cari username lain!
    if(user){
        return res.json({message:'User already exists!'});
    }

    try {
        // kalau gak ada ya udah mulai proses registrasi.
        // 1. hash password
        const hashedPassword = await bcrypt.hash(password,10)
        // 2. buat data user
        const newUser = new UserModel({username:username, password:hashedPassword})
        await newUser.save()
        res.json({message:'User Registered Successfully'})
    }
    catch(err){
        console.error(err)
        res.json(err)
    }

})
router.post('/login', async(req,res) => {
    const {username, password} = req.body
    try{
        // cek apakah ada user dari username tersebut
        const user = await UserModel.findOne({username:username})
        if(!user){
            return res.json({message:'That user does not exist!'})
        }

        // cek apakah password valid dengan komparasi bcrypt
        const isPasswordValid = await bcrypt.compare(password, user.password)
        if(!isPasswordValid){
            return res.json({message:'Username or password is incorrect!'})
        }

        // kalau username dan password valid
        const token = jwt.sign({id:user._id}, "secret")
        res.json({token, userID:user._id})
    }
    catch(err){
        console.error(err)
        res.json(err)
    }
})

export {router as userRouter}