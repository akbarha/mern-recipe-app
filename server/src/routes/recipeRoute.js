import RecipeModel from "../models/recipes.js";
import express from "express";
import UserModel from "../models/user.js";
import { verifyToken } from "../middleware/verifyToken.js";

const router = express.Router()

// get all recipes
router.get('/', async(req,res)=>{
    try{
        const recipes = await RecipeModel.find({})
        res.json(recipes)
    }
    catch(err){
        console.error(err)
        res.json(err)
    }
})


// buat recipe baru
router.post('/', verifyToken, async(req,res)=>{
    const recipe = new RecipeModel({
        ...req.body
    })
    try{
        const response = await recipe.save()
        res.json(response)
    }
    catch(err){
        console.error(err)
        res.json(err)
    }
})

// save recipe
router.put("/", verifyToken, async(req,res) => {
    const {recipeID, userID} = req.body
    try{

        // cari resepnya
        const recipe = await RecipeModel.findById(recipeID)
        if(!recipe){
            res.status(404).json({info:'No Recipe Found'})
        }

        // cari usernya
        const user = await UserModel.findById(userID)
        if(!user){
            res.status(404).json({info:'No User Found'})
        }

        // save resep di user
        user.savedRecipes.push(recipe)
        await user.save()
        res.json({savedRecipes:user.savedRecipes})
    }
    catch(err){
        console.error(err)
        res.json(err)
    }
    
})

router.get('/savedRecipes/ids/:userID', async(req,res)=>{
    try{
        const {userID} = req.params
        const user = await UserModel.findById(userID)
        if(!user){
            res.status(404).json({info:'No User Found!'})
        }

        res.json({savedRecipes:user?.savedRecipes})

    }
    catch(err){
        console.error(err)
        res.json(err)
    }
})

router.get('/savedRecipes/:userID', async(req,res)=>{
    try{
        const {userID} = req.params
        const user = await UserModel.findById(userID)
        if(!user){
            res.status(404).json({info:'No User Found!'})
        }
        const savedRecipes = await RecipeModel.find({
            _id:{$in:user.savedRecipes}
        })
        res.json({savedRecipes})

    }
    catch(err){
        console.error(err)
        res.json(err)
    }
})

export {router as RecipeRouter}