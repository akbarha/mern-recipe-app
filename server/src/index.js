import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import {userRouter} from './routes/userRoute.js'
import {RecipeRouter} from './routes/recipeRoute.js'

const app = express()
const mongoURI = 'mongodb://localhost:27017/recipe'
const mongooseOptions = {
    useNewUrlParser:true,
    useUnifiedTopology:true
}

app.use(express.json())
app.use(cors())

// mongoose with mongodb connect
main().catch(err => console.log(err))
async function main(){
    await mongoose.connect(mongoURI)
    console.log('db connected')
}

app.use('/auth', userRouter)
app.use('/recipes', RecipeRouter)

app.listen(3333, () => {
    console.log('Server backend di 3333')
})